import 'package:flutter/material.dart';

class Assignment3 extends StatefulWidget {
  const Assignment3({super.key});
  @override
  State<Assignment3> createState() => _Assignment3State();
}

class _Assignment3State extends State<Assignment3> {
  int? index = 0;

  final List<String> imageList = [
    "https://picsum.photos/250?image=9",
    "https://docs.flutter.dev/assets/images/dash/dash-fainting.gif",
    "https://docs.flutter.dev/assets/images/docs/cookbook/fading-in-images.gif"
  ];
  void showNextImage() {
    setState(() {
      int len = imageList.length;
      if (index! == len - 1) {
        index = 0;
      } else {
        index = index! + 1;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Display Image"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.network(imageList[index!], height: 420, width: 300),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: showNextImage,
              child: const Text("next"),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: () {
                setState(() {
                  index = 0;
                });
              },
              child: const Text(
                "reset",
              ),
            ),
          ],
        ),
      ),
    );
  }
}
