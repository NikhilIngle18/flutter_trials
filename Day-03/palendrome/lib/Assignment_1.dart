import 'package:flutter/material.dart';

class Assignment1 extends StatefulWidget {
  const Assignment1({super.key});
  @override
  State<Assignment1> createState() => _Assignment1State();
}

class _Assignment1State extends State<Assignment1> {
  final List<int> nums = [1001, 2345, 223, 5432, 1221, 145, 153];
  int? _count = 0;
  int? armstrongCount = 0;
  int? strongCount = 0;
  void reset() {
    setState(() {
      _count = 0;
      armstrongCount = 0;
      strongCount = 0;
    });
  }

  void strong() {
    strongCount = 0;
    for (int i = 0; i < nums.length; i++) {
      int temp = nums[i];
      int sum = 0;
      while (temp != 0) {
        int rem = temp % 10;
        int fact = 1;
        for (int j = 1; j <= rem; j++) {
          fact = fact * j;
        }
        sum = sum + fact;
        temp = temp ~/ 10;
      }
      setState(() {
        if (sum == nums[i]) {
          strongCount = strongCount! + 1;
        }
      });
    }
  }

  void palendrome() {
    _count = 0;
    setState(() {
      for (int i = 0; i < nums.length; i++) {
        int temp = nums[i];
        int val = nums[i];
        int rev = 0;
        while (temp != 0) {
          int rem = temp % 10;
          rev = rev * 10 + rem;
          temp = temp ~/ 10;
        }
        if (rev == val) {
          _count = _count! + 1;
        }
      }
    });
  }

  int countNum(int temp, [cnt = 0]) {
    while (temp != 0) {
      cnt++;
      temp = temp ~/ 10;
    }
    return cnt;
  }

  void armstrong() {
    armstrongCount = 0;
    setState(() {
      for (int i = 0; i < nums.length; i++) {
        int temp = nums[i];
        int cnt = countNum(temp);

        int sum = 0;
        while (temp != 0) {
          int rem = temp % 10;
          int arm = 1;
          for (int i = 0; i < cnt; i++) {
            arm = arm * rem;
          }
          sum = sum + arm;
          temp = temp ~/ 10;
        }

        if (sum == nums[i]) {
          armstrongCount = armstrongCount! + 1;
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            "NUMBERS",
          ),
        ),
        body: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                "click to print count of palendrome:",
              ),
              const SizedBox(
                height: 20,
              ),
              Text(
                "$_count",
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                onPressed: palendrome,
                child: const Text(
                  "palendrome",
                ),
              ),
            ],
          ),
          SizedBox(
            width: 20,
            height: 20,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                "click to print count of Armstrong",
              ),
              const SizedBox(
                height: 20,
              ),
              Text(
                "$armstrongCount",
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                onPressed: armstrong,
                child: const Text(
                  "Armstrong",
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                onPressed: reset,
                child: const Text(
                  "Reset",
                ),
              ),
            ],
          ),
          SizedBox(height: 20, width: 20),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                "click to print count of palendrome:",
              ),
              const SizedBox(
                height: 20,
              ),
              Text(
                "$strongCount",
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                onPressed: strong,
                child: const Text(
                  "strong",
                ),
              ),
            ],
          ),
        ]));
  }
}
