import 'package:flutter/material.dart';

class Assignment4 extends StatelessWidget {
  const Assignment4({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: SingleChildScrollView(
      child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Container(
          height: 200,
          width: 120,
          color: Colors.black54,
        ),
        const SizedBox(
          height: 20,
        ),
        Container(
          height: 200,
          width: 120,
          color: Colors.redAccent,
        ),
      ]),
    )));
  }
}
