import 'package:flutter/material.dart';

class Assignment6 extends StatelessWidget {
  const Assignment6({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: SingleChildScrollView(
      child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Container(
          height: 200,
          width: 120,
          color: Colors.black54,
        ),
        const SizedBox(
          height: 20,
        ),
        Container(
          height: 200,
          width: 120,
          color: Colors.redAccent,
        ),
        const SizedBox(
          height: 20,
        ),
        Container(
          height: 200,
          width: 120,
          color: Colors.black54,
        ),
        const SizedBox(
          height: 20,
        ),
        Container(
          height: 200,
          width: 120,
          color: Colors.redAccent,
        ),
        const SizedBox(
          height: 20,
        ),
        Container(
          height: 200,
          width: 120,
          color: Colors.black54,
        ),
        const SizedBox(
          height: 20,
        ),
        Container(
          height: 200,
          width: 120,
          color: Colors.redAccent,
        ),
      ]),
    )));
  }
}
