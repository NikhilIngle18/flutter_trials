import 'package:flutter/material.dart';
import 'Assignment_1.dart';
import 'Assignment_2.dart';
import 'Assignment_3.dart';
import 'Assignment_4.dart';
import 'Assignment_6.dart';
import 'Assignment_7.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Assignment1(),
    );
  }
}
