import 'package:flutter/material.dart';

class Assignment7 extends StatelessWidget {
  const Assignment7({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black38,
      body: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
          const SizedBox(
            width: 10,
          ),
          Image.network(
            "https://images.unsplash.com/photo-1623785419758-7d48241054a4?q=80&w=1000&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8N3x8c3VwZXJiaWtlfGVufDB8fDB8fHww",
            width: 150,
            height: 300,
          ),
          const SizedBox(
            width: 10,
          ),
          Image.network(
            "https://images.unsplash.com/photo-1595691403533-7f4a52a5b189?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8c3VwZXJiaWtlfGVufDB8fDB8fHww",
            width: 150,
            height: 300,
          ),
          const SizedBox(
            width: 10,
          ),
          Image.network(
            "https://images.unsplash.com/photo-1589560551995-babb81aa192d?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8c3VwZXJiaWtlfGVufDB8fDB8fHww",
            width: 150,
            height: 300,
          ),
          const SizedBox(
            width: 10,
          ),
          Image.network(
            "https://images.unsplash.com/photo-1631612428126-75b988d3950e?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTJ8fHN1cGVyYmlrZXxlbnwwfHwwfHx8MA%3D%3D",
            width: 150,
            height: 300,
          ),
          const SizedBox(
            width: 10,
          ),
          Image.network(
            "https://images.unsplash.com/photo-1547729966-d80039abb615?w=600&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mjh8fHN1cGVyYmlrZXxlbnwwfHwwfHx8MA%3D%3D",
            width: 150,
            height: 300,
          ),
          const SizedBox(
            width: 10,
          ),
        ]),
      ),
    );
  }
}
