import 'package:flutter/material.dart';

class Assignment4 extends StatefulWidget {
  const Assignment4({super.key});

  @override
  State<Assignment4> createState() => _Assignment4State();
}

class _Assignment4State extends State<Assignment4> {
  bool _ispost1like = false;
  bool _ispost2like = false;
  bool _ispost3like = false;
  bool _ispost4like = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: const Text(
          "Instagram",
          style: TextStyle(
            fontStyle: FontStyle.normal,
            color: Color.fromARGB(255, 185, 21, 218),
            fontSize: 30,
          ),
        ),
        actions: const [
          Icon(
            Icons.favorite_rounded,
            color: Colors.red,
          ),
        ],
      ),

      // // body: ListView(
      //   children: [
      //     Column(
      //       mainAxisAlignment: MainAxisAlignment.start,
      //       crossAxisAlignment: CrossAxisAlignment.center,
      //       children: [
      //         Container(
      //           color: Colors.indigoAccent,
      //           child: Image.network(
      //             "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBESERISEhYYGBgYGRERGBkaGBgSGBISGBgZGRgYGBgcIS4lHB4rHxgYJjgmKy8xNTU1GiQ7QDs0Py40NTEBDAwMEA8QGhESHjQsISE0NDQ0NDE0NDQ0NDE0MTE0NDQ0NDQ0NDQ0MTQ0NDQxNDQ0NDQ0NDQ0NDQxNDQ0MTQ0NP/AABEIALcBEwMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAABBAACAwUGB//EAEIQAAIBAgMEBgcEBwgDAAAAAAABAhESAyExBAUTQVFhcYGR0QYiUpKhscEUMtLwFUJDU2KC4SMzcpOiwtPxY6Pi/8QAGQEBAQEBAQEAAAAAAAAAAAAAAAECAwQF/8QAHxEBAQEBAQACAwEBAAAAAAAAABEBAhIDQSFRYRMx/9oADAMBAAIRAxEAPwDJRComiiFRPq18aM1ENpqohtFIytComiiWtFWM7QqJoohtFIzUQ2mlpa0lVkohUTVRCoikZWllEuohoSrFLSWmtpLRSM7Q2mlpLRSM7SWmtpLSUjK0NDWhLRVjKha0vaS0UilpLS9obRVZ2ktNLSWijO0lpraS0lIyUQ2mloaCqztDaaWhtJVjG0htaAUjmKJZRNFEKiarnFFEKiaKIUhSKWhUS6iFRFIpaRRNFENoqxnQtaXUSyiKRlaG01oFRJVjK0sol1ENopFLSWmlobSUjOhKGtpLRVjO0NppaS0UjO0lppaS0lIztJaa0JQVYytJaa0JQUjK0NppQNpKRlaS01UQqApGSgS02UTh7/8ASPB2SsPv4nsJ5Ryyvf6vZr8yelzHXtORtnpJsmFGvEjN6KMGptvtTou9ngd5ekG1bQnGc6QesIKyD6mlnJdrZy0T035e8fptH9z44iT7/VIeF4jIKeX1pRLKJoohtN1zjO0Kia2kUSUiiiFRNFEKiPRFFENpoollEeiMrQqJraG0no8srQqJraS0ejyzoG0X/SWBdbeu39Xx+ug3CUZKsWmulNNeKG3FzMVtDaDHx4YdHOSjXSr1OJtXpDRyWHBNaKUq59dvQMzd/wCGzHctJaePW2zUnJznWXrNqTWfdyOrsu+ZxosT1ksqpes101rnT8s1vO4mdY7lpLTDZ944c6Z0rlV0Su9nWteea0G4NPRp9jT+RhqM7SWkw8aE5SjGSbjk0nVpm1oIxtJabWgtJVjK0NpraSgpGVobTShKCrGdpXFnGEXKbUYrNttRSXS29Di+knpPhbJ6kVfitJqH6sU9HN8tPurPsTqfNt470x9pldjTlLOqWkI9FsFkte0VfL1XpH6Y3KWFsjaWali6Nroh0f4vDpPFSk288+fawFWyNZyNSssRIyljdBg5GN6/Tecfttx2EXqQz61vzj309sxVrOb1/Xl5g2beePh1sm6OuT9ZdylWhhJPu8a/mprDsPc8Dt7L6Qyp/awXbCq/0utfE6D33gUTi26/wtW9taV7EeUnGoFNLl3md5xfWvbT2xJ0glPJSTU4U+dUa4e0xrSVE8s61i66Ul9HzPD4UtHpTmlp0M6mDvOKSuiq5JuKj660d0Xl59RjeWs6zXrlEjVNcu3Kp5ae1yxM1NLK1xT4ba6JVea7zKSadGmuqlMhnH9N6ewS5d/cGh4t0/PIYe8MVwsc5U01z8dX4jxv7Pb1EsaCurKKtzlWSVvb0Hnd671eJXDw8oaN88TyXV/0cxrPQkvz/Q1zzmflnrrdVeJlR9oYY84OsJShWjybVe2moXFPMylA2y0x8ec3WcnJ0pVtvKuSz7zGjNIwVaB5UDLNRkxiMykPzyRssKdjxFFuKai2v1W9K9HaKsZXNaZxqm11/ll8LanB1g5Rejo7aru7wxgm669Wa+X0Kyg1yy8fiRV4TTaaya0p5mz2jEatc5taNXSpTopUSnjQgnWSr0asWntr5ZLrzk/IEdbB2rEw/uTcVzzovDQbwPSGUVSThPr0fPNtZHlp7Q5vNunWXjiImzWsuPX7P6QUcuKqrWNmvZRvNdf5Qx/SWNP7ODu/ieS7UtTyM9rjBZunb9FzEsfflMoRTfTLyRnc5az1r1eN6SYkE3iTsTeqSk1lpGNKtaVOBvD0yx5QswpTjm3e5O5rqXLPpb7Eea2naZ4knKcqv5dSXIxlNHPrrPp15537XnJtuUm2222223JvVtvNsrKRjLEKSlU57065yvPF6DKU2wMFTG7WsyIwEYCKNSAoQK7ez7zmlRvul6y8zp7PvJOl6745rwPKqpeGJJaN+J15+Xccuvh517WGLGSrF1/PQGMo1oePhtc1zT7RzA3xiR+8lJd6fidM+bHLfg36elVG+w1hO3NNrl0VODDfmG/vQlHspJfQZjvbBf61O1PyNe+f2xvx9Z9Oy8eDpWNOtaPtX1XhzLTxVVrDbtedHna+hP6nMhtmE9Jx8UhiE06OLT7HUudJvOm1tC0kn3dIZ40ElRyrzi46fzV+FBfN5shank1CVdA2i8MtDeOPTUvog0YFFGc8eT0y+ZRKXSyejyZoirSaM4xkXSHo8NYZUzOjsW1xUaOUVyq0oZPX16Zd+WWfI5E5001Fpwk9XUz1uavObhzbtvw8uHFtqqbooReetKa/Dopoc7H2rFmqSlK3W1NpV7OZpwwSnCLo5JErUKrCZfhNmn2qC517EZYu8sOPT8h6XwPDoqvJdeQptG3KOUM+t6dy5ie2bwc31clyXmIYmPUxvbefGvj47k23mxWU2aQdWlVd+SKYiSbzr1rT4nPeq6ZxmKNsq0yzaBRv80M10ijAaRg3oq9mYKJEGdCKJdyK3AS0DRHIFwEqyAqQA3g4h0vs0v3D8Zr6Bexy/cP3p+RqJcc64Nx0fsMueCvekXjsUv3Mffl5ialxzL2FTZ1VsfThR96XmSWxwWsILnnOX4hC45ikGE6aOj6sjpLZsL+D3v6l44GDX9n739SzU9YWwtvxIZrEl2NuXweQ3DfeKtZRf8vlQ1hs+D/4fH/6GYbBhNZQwn2Jv/cazOv2zu8/eMMPf8ucYvsrHzGIb8VaOHhJP6G8N2w9iHuL5m/2GDVHCHuRX0LOmLwzhvjByurGrazWVVTmq9KHcPbMNqqlGnaL/YMP2Y+5DyNFseE9YQfbCH4TWevtN8/TZ7Zh+1HxRR7bD2o+8iv2LB/d4f8Alw/CH7FhexBfyQ8i/lPwH2vD9uHvR8zPG3jgxX3ot9Ckn/0b/ZcP2Ie5D8JX7BhtafCPkTauRxMbblN1c12VVF3GEtphRu5eKTfidmexRWj/ANMPwlHsv8T8I+RmatxwcTaqqql3KmnbWvwFpYizbf1bPRT2RNNNt9qg/oYS3bh/lQ8ibzredY89iTVXStOvUrU7s9gguX+mD/2mctjjX7q92H4TPnVzrHGkqJO5Z55OrXalzKSlHk/z1Ha+yw9mPuxX0M57NFaJeCMzWvWOZolW3PxXkWjKCVU7u7JD6wY+zEnAXsLwZI1nWOe9sk8k0l8CicK5y68tPH+h0HgrlFfHzKKDT0+fmIeiTxoLSKfW238MjKWMnkkl3JHSlGT6fF+ZVOa0lPo+9LTxELjnXrn5/LILa6V8Pyh9QmtK/EDhLofgFuOdd1gOhwpey/BkCF0sR+0/Fk4U/Zl4M78YPr+JtCEvyzt/n/Xn/wBf4808CXsy91+RVwS1XwPXwgaxi+vxH+X9P9/48lDYcSWkH3q35jOHujEeqjHtfkeoUe02jBdHwNZ8WJvza8zDcr5zXcqjOHubDX3nKXgjv8N8rV3V8i3Cll93woXOMZ35enKwt34MH9xV6/W+Y3GiyXwGeDNv9XwLTg1Dld12Up3mvOZ/xj3u/wDS8Zt6G8MNvXIwWMq9fUo/Qsp88/Fr4Fh6aTw5rR1+Zmpz6GG9/lgWIhD0vGci6mUWIgPESJ5PTSbqsnQXniTjrXxdDaOIdPYIJpyrCq9Z6Nxj0uuUdNWTcmLm3Y4fHfSDiVHNtwMGb9TKWbbi/V8Gs+3Ls5vnY+xYkE2qSS6Gq+7WvgSL6xpl0gcK6Sa91/NCLxGsnXvyLcWmYi+mW2QxoZqdV02Qy7fVEJ7VjLnH3IP/AGnWjtBjjYMJZr1X1ad6M7y1nTmreOLXOz3IeQZ72xKulvuL56lNpwHHXx5MTlE57m4687zp79LYvRB9sZfSRFvnEX7PDf8ALP8AEIUBUzdb/DofpiX7rC8MT8YP0zLlhYS7sT8ZzmyrJdJjprfEv3WE/wDN+mIFb6p+wwf/AH/8pyakvH5WOtLfdf2GF3S2lfLGKrfVP2GF7+1f8xy7wVA636cf7nC97aP+Uhx8yCD2agaQiL8eXKhSO0Yl1EqvoSqz2PBXRiaxXV8jPY9j2nEo3BRWtZeqvDX4HVjuiVv9563+FKPmLhCai8jWMX1D0t3ZQspVKjuq3J9OTouw1wdi9voSonq+brTLsM+iEI4bpnSvw8P6gWE+lfFHcWFClKfQq9mg9U3/ADS8xTcchR6l+e4Dh/BF9x2ls2H7PxfmFbPh+z8X5ikefnhz5Rh3oSxtmxG6te6lFfM9d9nw/ZXi/M5G99hmq4mG240zhk3HrXSurUudG446hJfqv5/Uo4T9lkWLOjdfkCHFm6Yd8uyra8NDbK6hPW1lWn0PwYdpwsbDS4imq1Sq2K8SWtX4gNRquTXcy3G9W250bUqLRtaNrvErnrmMQToCtliU7OdM2Uni15/VlVCU600VK9+gzs+7sTEdsYSWmbTiku1gYKVap59uZlLY1ydOrVDsdlxOI8OMG5J5qn3eVa6U69B2O58fVpc8rkS4fl56exyjms+zyBFLop8D0eBufGlqlDtafgonQ2bcsF/eScn0L1V5k3cxrM15HgZZrLs+Ytibuw5dMX0ryPfbNuzCw5OSrKqolKkkvhma4uwYE004R7lRrsa0M7uLlx8p2vYJ4ebzj7S+q5Cjij6otx4SbacurR07a6nN2z0QwZxdsrJ8pKPq06JQrR91Dn1zn07c9b9vncsMynBo6m9N24uzTsxF0uMl92cemL71VchFmN5dM0pIqMSgjGeG0ZjpmqMFSEDQ1IAgSPrzwYN1cI1/woOFhQg24QjFvVpJGKmG89UfPptYgbxTiB4ghTamFTFOIFYgiU2phWIJ3hUxCnFiFuIJXhvEKcWIHiCamHiEh6GewYMpXOCrq6VSb60hqFsVSKSXQkkvgK8QPELCmcRRkqSSa6Gqo5O2bjhiScoSsrqkqxr0pZU5j14bxIV5ue6sVTsscqZpqlsk+tnR2Tc0sniNJV+6s2l2rL5nTvDeXaZF8DZcPDpbFKlXz5/PKiqxjiCt4eIZi01eS8V4hOISFM8Ql4txCcQQpi8l4vxCcQQpi8FxhxCXiLQ23ZsPGg4YkVKL5Pk+lPVPrR4zeXofiRrLAkpr2JUjNLqekvge0vJcN5azvcfJcfBnhzlCcXGUcnF5NGR9O3vurC2qFJ5SVbZr70fOPUeB3rujF2aXrqsXlGUfuvqfQ+p/E57zHbnvOnMlhpmEoNDJGZjpnUKEGeFEhI36x9HWIWvFlMtcet8wxeG8VuDeAzeHiC15LwGuIHiCt4eIUM8QN4rxA8QBq8l4rxA8QJTV4bxVTDeCmuIHiCt4bwtNcQPEFLw8QhTXEJeLcQKxAGbyXi3EJxBA1eS8WuJcItM3huFriXCFM3BuFrw3EgZUw3iymG8Qre8piKMk4ySaaaaeaafJlLwXki5rhbb6K4M6vClKDzdH60eyjzXied2jcO1Qq3huSVc4tTTXTROtO499cG8zvx5rpz8vWPmNvWvAh9NqiDw1/q4axCyxBRTCpnR5zamBzF1iBcwN+ITiC15Lyhq8N4pxA8QJTV4bxXiE4gKbvDxBO8N4U2sQPEFFMspgNcQKmK8QPEAavDeKcQPEAbvJeK3hvAb4hOIK3hvAZ4hLxa8PEAZvDcK8QPEAavDeK8QnEAavCpit4bwpriEuFryXgM3BvFrg3AM3kFuIQg4NxFIhCMLKQbyEArcS4hCg3kvIQrQ3kvIQCXkvIQMipFlMJAoXhuIQIN4byEAN4byEAN5LyEDSXhvCQCKYbyEAN5LyEAN4byEANwbgECjeFTIQCXkIQD//2Q==",
      //             width: double.infinity,
      //             height: 200,
      //           ),
      //         ),
      //         Row(
      //           children: [
      //             IconButton(
      //                 onPressed: () {},
      //                 icon: const Icon(
      //                   Icons.favorite_outline_rounded,
      //                 )),
      //             IconButton(
      //                 onPressed: () {},
      //                 icon: const Icon(
      //                   Icons.comment_bank_outlined,
      //                 )),
      //             IconButton(
      //               onPressed: () {},
      //               icon: const Icon(Icons.send),
      //             ),
      //           ],
      //         ),
      //       ],
      //     ),
      //     Column(
      //       mainAxisAlignment: MainAxisAlignment.start,
      //       crossAxisAlignment: CrossAxisAlignment.center,
      //       children: [
      //         Container(
      //           color: Colors.indigoAccent,
      //           child: Image.network(
      //             "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBESERISEhYYGBgYGRERGBkaGBgSGBISGBgZGRgYGBgcIS4lHB4rHxgYJjgmKy8xNTU1GiQ7QDs0Py40NTEBDAwMEA8QGhESHjQsISE0NDQ0NDE0NDQ0NDE0MTE0NDQ0NDQ0NDQ0MTQ0NDQxNDQ0NDQ0NDQ0NDQxNDQ0MTQ0NP/AABEIALcBEwMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAABBAACAwUGB//EAEIQAAIBAgMEBgcEBwgDAAAAAAABAhESAyExBAUTQVFhcYGR0QYiUpKhscEUMtLwFUJDU2KC4SMzcpOiwtPxY6Pi/8QAGQEBAQEBAQEAAAAAAAAAAAAAAAECAwQF/8QAHxEBAQEBAQACAwEBAAAAAAAAABEBAhIDQSFRYRMx/9oADAMBAAIRAxEAPwDJRComiiFRPq18aM1ENpqohtFIytComiiWtFWM7QqJoohtFIzUQ2mlpa0lVkohUTVRCoikZWllEuohoSrFLSWmtpLRSM7Q2mlpLRSM7SWmtpLSUjK0NDWhLRVjKha0vaS0UilpLS9obRVZ2ktNLSWijO0lpraS0lIyUQ2mloaCqztDaaWhtJVjG0htaAUjmKJZRNFEKiarnFFEKiaKIUhSKWhUS6iFRFIpaRRNFENoqxnQtaXUSyiKRlaG01oFRJVjK0sol1ENopFLSWmlobSUjOhKGtpLRVjO0NppaS0UjO0lppaS0lIztJaa0JQVYytJaa0JQUjK0NppQNpKRlaS01UQqApGSgS02UTh7/8ASPB2SsPv4nsJ5Ryyvf6vZr8yelzHXtORtnpJsmFGvEjN6KMGptvtTou9ngd5ekG1bQnGc6QesIKyD6mlnJdrZy0T035e8fptH9z44iT7/VIeF4jIKeX1pRLKJoohtN1zjO0Kia2kUSUiiiFRNFEKiPRFFENpoollEeiMrQqJraG0no8srQqJraS0ejyzoG0X/SWBdbeu39Xx+ug3CUZKsWmulNNeKG3FzMVtDaDHx4YdHOSjXSr1OJtXpDRyWHBNaKUq59dvQMzd/wCGzHctJaePW2zUnJznWXrNqTWfdyOrsu+ZxosT1ksqpes101rnT8s1vO4mdY7lpLTDZ944c6Z0rlV0Su9nWteea0G4NPRp9jT+RhqM7SWkw8aE5SjGSbjk0nVpm1oIxtJabWgtJVjK0NpraSgpGVobTShKCrGdpXFnGEXKbUYrNttRSXS29Di+knpPhbJ6kVfitJqH6sU9HN8tPurPsTqfNt470x9pldjTlLOqWkI9FsFkte0VfL1XpH6Y3KWFsjaWali6Nroh0f4vDpPFSk288+fawFWyNZyNSssRIyljdBg5GN6/Tecfttx2EXqQz61vzj309sxVrOb1/Xl5g2beePh1sm6OuT9ZdylWhhJPu8a/mprDsPc8Dt7L6Qyp/awXbCq/0utfE6D33gUTi26/wtW9taV7EeUnGoFNLl3md5xfWvbT2xJ0glPJSTU4U+dUa4e0xrSVE8s61i66Ul9HzPD4UtHpTmlp0M6mDvOKSuiq5JuKj660d0Xl59RjeWs6zXrlEjVNcu3Kp5ae1yxM1NLK1xT4ba6JVea7zKSadGmuqlMhnH9N6ewS5d/cGh4t0/PIYe8MVwsc5U01z8dX4jxv7Pb1EsaCurKKtzlWSVvb0Hnd671eJXDw8oaN88TyXV/0cxrPQkvz/Q1zzmflnrrdVeJlR9oYY84OsJShWjybVe2moXFPMylA2y0x8ec3WcnJ0pVtvKuSz7zGjNIwVaB5UDLNRkxiMykPzyRssKdjxFFuKai2v1W9K9HaKsZXNaZxqm11/ll8LanB1g5Rejo7aru7wxgm669Wa+X0Kyg1yy8fiRV4TTaaya0p5mz2jEatc5taNXSpTopUSnjQgnWSr0asWntr5ZLrzk/IEdbB2rEw/uTcVzzovDQbwPSGUVSThPr0fPNtZHlp7Q5vNunWXjiImzWsuPX7P6QUcuKqrWNmvZRvNdf5Qx/SWNP7ODu/ieS7UtTyM9rjBZunb9FzEsfflMoRTfTLyRnc5az1r1eN6SYkE3iTsTeqSk1lpGNKtaVOBvD0yx5QswpTjm3e5O5rqXLPpb7Eea2naZ4knKcqv5dSXIxlNHPrrPp15537XnJtuUm2222223JvVtvNsrKRjLEKSlU57065yvPF6DKU2wMFTG7WsyIwEYCKNSAoQK7ez7zmlRvul6y8zp7PvJOl6745rwPKqpeGJJaN+J15+Xccuvh517WGLGSrF1/PQGMo1oePhtc1zT7RzA3xiR+8lJd6fidM+bHLfg36elVG+w1hO3NNrl0VODDfmG/vQlHspJfQZjvbBf61O1PyNe+f2xvx9Z9Oy8eDpWNOtaPtX1XhzLTxVVrDbtedHna+hP6nMhtmE9Jx8UhiE06OLT7HUudJvOm1tC0kn3dIZ40ElRyrzi46fzV+FBfN5shank1CVdA2i8MtDeOPTUvog0YFFGc8eT0y+ZRKXSyejyZoirSaM4xkXSHo8NYZUzOjsW1xUaOUVyq0oZPX16Zd+WWfI5E5001Fpwk9XUz1uavObhzbtvw8uHFtqqbooReetKa/Dopoc7H2rFmqSlK3W1NpV7OZpwwSnCLo5JErUKrCZfhNmn2qC517EZYu8sOPT8h6XwPDoqvJdeQptG3KOUM+t6dy5ie2bwc31clyXmIYmPUxvbefGvj47k23mxWU2aQdWlVd+SKYiSbzr1rT4nPeq6ZxmKNsq0yzaBRv80M10ijAaRg3oq9mYKJEGdCKJdyK3AS0DRHIFwEqyAqQA3g4h0vs0v3D8Zr6Bexy/cP3p+RqJcc64Nx0fsMueCvekXjsUv3Mffl5ialxzL2FTZ1VsfThR96XmSWxwWsILnnOX4hC45ikGE6aOj6sjpLZsL+D3v6l44GDX9n739SzU9YWwtvxIZrEl2NuXweQ3DfeKtZRf8vlQ1hs+D/4fH/6GYbBhNZQwn2Jv/cazOv2zu8/eMMPf8ucYvsrHzGIb8VaOHhJP6G8N2w9iHuL5m/2GDVHCHuRX0LOmLwzhvjByurGrazWVVTmq9KHcPbMNqqlGnaL/YMP2Y+5DyNFseE9YQfbCH4TWevtN8/TZ7Zh+1HxRR7bD2o+8iv2LB/d4f8Alw/CH7FhexBfyQ8i/lPwH2vD9uHvR8zPG3jgxX3ot9Ckn/0b/ZcP2Ie5D8JX7BhtafCPkTauRxMbblN1c12VVF3GEtphRu5eKTfidmexRWj/ANMPwlHsv8T8I+RmatxwcTaqqql3KmnbWvwFpYizbf1bPRT2RNNNt9qg/oYS3bh/lQ8ibzredY89iTVXStOvUrU7s9gguX+mD/2mctjjX7q92H4TPnVzrHGkqJO5Z55OrXalzKSlHk/z1Ha+yw9mPuxX0M57NFaJeCMzWvWOZolW3PxXkWjKCVU7u7JD6wY+zEnAXsLwZI1nWOe9sk8k0l8CicK5y68tPH+h0HgrlFfHzKKDT0+fmIeiTxoLSKfW238MjKWMnkkl3JHSlGT6fF+ZVOa0lPo+9LTxELjnXrn5/LILa6V8Pyh9QmtK/EDhLofgFuOdd1gOhwpey/BkCF0sR+0/Fk4U/Zl4M78YPr+JtCEvyzt/n/Xn/wBf4808CXsy91+RVwS1XwPXwgaxi+vxH+X9P9/48lDYcSWkH3q35jOHujEeqjHtfkeoUe02jBdHwNZ8WJvza8zDcr5zXcqjOHubDX3nKXgjv8N8rV3V8i3Cll93woXOMZ35enKwt34MH9xV6/W+Y3GiyXwGeDNv9XwLTg1Dld12Up3mvOZ/xj3u/wDS8Zt6G8MNvXIwWMq9fUo/Qsp88/Fr4Fh6aTw5rR1+Zmpz6GG9/lgWIhD0vGci6mUWIgPESJ5PTSbqsnQXniTjrXxdDaOIdPYIJpyrCq9Z6Nxj0uuUdNWTcmLm3Y4fHfSDiVHNtwMGb9TKWbbi/V8Gs+3Ls5vnY+xYkE2qSS6Gq+7WvgSL6xpl0gcK6Sa91/NCLxGsnXvyLcWmYi+mW2QxoZqdV02Qy7fVEJ7VjLnH3IP/AGnWjtBjjYMJZr1X1ad6M7y1nTmreOLXOz3IeQZ72xKulvuL56lNpwHHXx5MTlE57m4687zp79LYvRB9sZfSRFvnEX7PDf8ALP8AEIUBUzdb/DofpiX7rC8MT8YP0zLlhYS7sT8ZzmyrJdJjprfEv3WE/wDN+mIFb6p+wwf/AH/8pyakvH5WOtLfdf2GF3S2lfLGKrfVP2GF7+1f8xy7wVA636cf7nC97aP+Uhx8yCD2agaQiL8eXKhSO0Yl1EqvoSqz2PBXRiaxXV8jPY9j2nEo3BRWtZeqvDX4HVjuiVv9563+FKPmLhCai8jWMX1D0t3ZQspVKjuq3J9OTouw1wdi9voSonq+brTLsM+iEI4bpnSvw8P6gWE+lfFHcWFClKfQq9mg9U3/ADS8xTcchR6l+e4Dh/BF9x2ls2H7PxfmFbPh+z8X5ikefnhz5Rh3oSxtmxG6te6lFfM9d9nw/ZXi/M5G99hmq4mG240zhk3HrXSurUudG446hJfqv5/Uo4T9lkWLOjdfkCHFm6Yd8uyra8NDbK6hPW1lWn0PwYdpwsbDS4imq1Sq2K8SWtX4gNRquTXcy3G9W250bUqLRtaNrvErnrmMQToCtliU7OdM2Uni15/VlVCU600VK9+gzs+7sTEdsYSWmbTiku1gYKVap59uZlLY1ydOrVDsdlxOI8OMG5J5qn3eVa6U69B2O58fVpc8rkS4fl56exyjms+zyBFLop8D0eBufGlqlDtafgonQ2bcsF/eScn0L1V5k3cxrM15HgZZrLs+Ytibuw5dMX0ryPfbNuzCw5OSrKqolKkkvhma4uwYE004R7lRrsa0M7uLlx8p2vYJ4ebzj7S+q5Cjij6otx4SbacurR07a6nN2z0QwZxdsrJ8pKPq06JQrR91Dn1zn07c9b9vncsMynBo6m9N24uzTsxF0uMl92cemL71VchFmN5dM0pIqMSgjGeG0ZjpmqMFSEDQ1IAgSPrzwYN1cI1/woOFhQg24QjFvVpJGKmG89UfPptYgbxTiB4ghTamFTFOIFYgiU2phWIJ3hUxCnFiFuIJXhvEKcWIHiCamHiEh6GewYMpXOCrq6VSb60hqFsVSKSXQkkvgK8QPELCmcRRkqSSa6Gqo5O2bjhiScoSsrqkqxr0pZU5j14bxIV5ue6sVTsscqZpqlsk+tnR2Tc0sniNJV+6s2l2rL5nTvDeXaZF8DZcPDpbFKlXz5/PKiqxjiCt4eIZi01eS8V4hOISFM8Ql4txCcQQpi8l4vxCcQQpi8FxhxCXiLQ23ZsPGg4YkVKL5Pk+lPVPrR4zeXofiRrLAkpr2JUjNLqekvge0vJcN5azvcfJcfBnhzlCcXGUcnF5NGR9O3vurC2qFJ5SVbZr70fOPUeB3rujF2aXrqsXlGUfuvqfQ+p/E57zHbnvOnMlhpmEoNDJGZjpnUKEGeFEhI36x9HWIWvFlMtcet8wxeG8VuDeAzeHiC15LwGuIHiCt4eIUM8QN4rxA8QBq8l4rxA8QJTV4bxVTDeCmuIHiCt4bwtNcQPEFLw8QhTXEJeLcQKxAGbyXi3EJxBA1eS8WuJcItM3huFriXCFM3BuFrw3EgZUw3iymG8Qre8piKMk4ySaaaaeaafJlLwXki5rhbb6K4M6vClKDzdH60eyjzXied2jcO1Qq3huSVc4tTTXTROtO499cG8zvx5rpz8vWPmNvWvAh9NqiDw1/q4axCyxBRTCpnR5zamBzF1iBcwN+ITiC15Lyhq8N4pxA8QJTV4bxXiE4gKbvDxBO8N4U2sQPEFFMspgNcQKmK8QPEAavDeKcQPEAbvJeK3hvAb4hOIK3hvAZ4hLxa8PEAZvDcK8QPEAavDeK8QnEAavCpit4bwpriEuFryXgM3BvFrg3AM3kFuIQg4NxFIhCMLKQbyEArcS4hCg3kvIQrQ3kvIQCXkvIQMipFlMJAoXhuIQIN4byEAN4byEAN5LyEDSXhvCQCKYbyEAN5LyEAN4byEANwbgECjeFTIQCXkIQD//2Q==",
      //             width: double.infinity,
      //             height: 200,
      //           ),
      //         ),
      //         Row(
      //           children: [
      //             IconButton(
      //                 onPressed: () {},
      //                 icon: const Icon(
      //                   Icons.favorite_outline_rounded,
      //                 )),
      //             IconButton(
      //                 onPressed: () {},
      //                 icon: const Icon(
      //                   Icons.comment_bank_outlined,
      //                 )),
      //             IconButton(
      //               onPressed: () {},
      //               icon: const Icon(Icons.send),
      //             ),
      //           ],
      //         ),
      //       ],
      //     ),
      //     Column(
      //       mainAxisAlignment: MainAxisAlignment.start,
      //       crossAxisAlignment: CrossAxisAlignment.center,
      //       children: [
      //         Container(
      //           color: Colors.indigoAccent,
      //           child: Image.network(
      //             "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBESERISEhYYGBgYGRERGBkaGBgSGBISGBgZGRgYGBgcIS4lHB4rHxgYJjgmKy8xNTU1GiQ7QDs0Py40NTEBDAwMEA8QGhESHjQsISE0NDQ0NDE0NDQ0NDE0MTE0NDQ0NDQ0NDQ0MTQ0NDQxNDQ0NDQ0NDQ0NDQxNDQ0MTQ0NP/AABEIALcBEwMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAABBAACAwUGB//EAEIQAAIBAgMEBgcEBwgDAAAAAAABAhESAyExBAUTQVFhcYGR0QYiUpKhscEUMtLwFUJDU2KC4SMzcpOiwtPxY6Pi/8QAGQEBAQEBAQEAAAAAAAAAAAAAAAECAwQF/8QAHxEBAQEBAQACAwEBAAAAAAAAABEBAhIDQSFRYRMx/9oADAMBAAIRAxEAPwDJRComiiFRPq18aM1ENpqohtFIytComiiWtFWM7QqJoohtFIzUQ2mlpa0lVkohUTVRCoikZWllEuohoSrFLSWmtpLRSM7Q2mlpLRSM7SWmtpLSUjK0NDWhLRVjKha0vaS0UilpLS9obRVZ2ktNLSWijO0lpraS0lIyUQ2mloaCqztDaaWhtJVjG0htaAUjmKJZRNFEKiarnFFEKiaKIUhSKWhUS6iFRFIpaRRNFENoqxnQtaXUSyiKRlaG01oFRJVjK0sol1ENopFLSWmlobSUjOhKGtpLRVjO0NppaS0UjO0lppaS0lIztJaa0JQVYytJaa0JQUjK0NppQNpKRlaS01UQqApGSgS02UTh7/8ASPB2SsPv4nsJ5Ryyvf6vZr8yelzHXtORtnpJsmFGvEjN6KMGptvtTou9ngd5ekG1bQnGc6QesIKyD6mlnJdrZy0T035e8fptH9z44iT7/VIeF4jIKeX1pRLKJoohtN1zjO0Kia2kUSUiiiFRNFEKiPRFFENpoollEeiMrQqJraG0no8srQqJraS0ejyzoG0X/SWBdbeu39Xx+ug3CUZKsWmulNNeKG3FzMVtDaDHx4YdHOSjXSr1OJtXpDRyWHBNaKUq59dvQMzd/wCGzHctJaePW2zUnJznWXrNqTWfdyOrsu+ZxosT1ksqpes101rnT8s1vO4mdY7lpLTDZ944c6Z0rlV0Su9nWteea0G4NPRp9jT+RhqM7SWkw8aE5SjGSbjk0nVpm1oIxtJabWgtJVjK0NpraSgpGVobTShKCrGdpXFnGEXKbUYrNttRSXS29Di+knpPhbJ6kVfitJqH6sU9HN8tPurPsTqfNt470x9pldjTlLOqWkI9FsFkte0VfL1XpH6Y3KWFsjaWali6Nroh0f4vDpPFSk288+fawFWyNZyNSssRIyljdBg5GN6/Tecfttx2EXqQz61vzj309sxVrOb1/Xl5g2beePh1sm6OuT9ZdylWhhJPu8a/mprDsPc8Dt7L6Qyp/awXbCq/0utfE6D33gUTi26/wtW9taV7EeUnGoFNLl3md5xfWvbT2xJ0glPJSTU4U+dUa4e0xrSVE8s61i66Ul9HzPD4UtHpTmlp0M6mDvOKSuiq5JuKj660d0Xl59RjeWs6zXrlEjVNcu3Kp5ae1yxM1NLK1xT4ba6JVea7zKSadGmuqlMhnH9N6ewS5d/cGh4t0/PIYe8MVwsc5U01z8dX4jxv7Pb1EsaCurKKtzlWSVvb0Hnd671eJXDw8oaN88TyXV/0cxrPQkvz/Q1zzmflnrrdVeJlR9oYY84OsJShWjybVe2moXFPMylA2y0x8ec3WcnJ0pVtvKuSz7zGjNIwVaB5UDLNRkxiMykPzyRssKdjxFFuKai2v1W9K9HaKsZXNaZxqm11/ll8LanB1g5Rejo7aru7wxgm669Wa+X0Kyg1yy8fiRV4TTaaya0p5mz2jEatc5taNXSpTopUSnjQgnWSr0asWntr5ZLrzk/IEdbB2rEw/uTcVzzovDQbwPSGUVSThPr0fPNtZHlp7Q5vNunWXjiImzWsuPX7P6QUcuKqrWNmvZRvNdf5Qx/SWNP7ODu/ieS7UtTyM9rjBZunb9FzEsfflMoRTfTLyRnc5az1r1eN6SYkE3iTsTeqSk1lpGNKtaVOBvD0yx5QswpTjm3e5O5rqXLPpb7Eea2naZ4knKcqv5dSXIxlNHPrrPp15537XnJtuUm2222223JvVtvNsrKRjLEKSlU57065yvPF6DKU2wMFTG7WsyIwEYCKNSAoQK7ez7zmlRvul6y8zp7PvJOl6745rwPKqpeGJJaN+J15+Xccuvh517WGLGSrF1/PQGMo1oePhtc1zT7RzA3xiR+8lJd6fidM+bHLfg36elVG+w1hO3NNrl0VODDfmG/vQlHspJfQZjvbBf61O1PyNe+f2xvx9Z9Oy8eDpWNOtaPtX1XhzLTxVVrDbtedHna+hP6nMhtmE9Jx8UhiE06OLT7HUudJvOm1tC0kn3dIZ40ElRyrzi46fzV+FBfN5shank1CVdA2i8MtDeOPTUvog0YFFGc8eT0y+ZRKXSyejyZoirSaM4xkXSHo8NYZUzOjsW1xUaOUVyq0oZPX16Zd+WWfI5E5001Fpwk9XUz1uavObhzbtvw8uHFtqqbooReetKa/Dopoc7H2rFmqSlK3W1NpV7OZpwwSnCLo5JErUKrCZfhNmn2qC517EZYu8sOPT8h6XwPDoqvJdeQptG3KOUM+t6dy5ie2bwc31clyXmIYmPUxvbefGvj47k23mxWU2aQdWlVd+SKYiSbzr1rT4nPeq6ZxmKNsq0yzaBRv80M10ijAaRg3oq9mYKJEGdCKJdyK3AS0DRHIFwEqyAqQA3g4h0vs0v3D8Zr6Bexy/cP3p+RqJcc64Nx0fsMueCvekXjsUv3Mffl5ialxzL2FTZ1VsfThR96XmSWxwWsILnnOX4hC45ikGE6aOj6sjpLZsL+D3v6l44GDX9n739SzU9YWwtvxIZrEl2NuXweQ3DfeKtZRf8vlQ1hs+D/4fH/6GYbBhNZQwn2Jv/cazOv2zu8/eMMPf8ucYvsrHzGIb8VaOHhJP6G8N2w9iHuL5m/2GDVHCHuRX0LOmLwzhvjByurGrazWVVTmq9KHcPbMNqqlGnaL/YMP2Y+5DyNFseE9YQfbCH4TWevtN8/TZ7Zh+1HxRR7bD2o+8iv2LB/d4f8Alw/CH7FhexBfyQ8i/lPwH2vD9uHvR8zPG3jgxX3ot9Ckn/0b/ZcP2Ie5D8JX7BhtafCPkTauRxMbblN1c12VVF3GEtphRu5eKTfidmexRWj/ANMPwlHsv8T8I+RmatxwcTaqqql3KmnbWvwFpYizbf1bPRT2RNNNt9qg/oYS3bh/lQ8ibzredY89iTVXStOvUrU7s9gguX+mD/2mctjjX7q92H4TPnVzrHGkqJO5Z55OrXalzKSlHk/z1Ha+yw9mPuxX0M57NFaJeCMzWvWOZolW3PxXkWjKCVU7u7JD6wY+zEnAXsLwZI1nWOe9sk8k0l8CicK5y68tPH+h0HgrlFfHzKKDT0+fmIeiTxoLSKfW238MjKWMnkkl3JHSlGT6fF+ZVOa0lPo+9LTxELjnXrn5/LILa6V8Pyh9QmtK/EDhLofgFuOdd1gOhwpey/BkCF0sR+0/Fk4U/Zl4M78YPr+JtCEvyzt/n/Xn/wBf4808CXsy91+RVwS1XwPXwgaxi+vxH+X9P9/48lDYcSWkH3q35jOHujEeqjHtfkeoUe02jBdHwNZ8WJvza8zDcr5zXcqjOHubDX3nKXgjv8N8rV3V8i3Cll93woXOMZ35enKwt34MH9xV6/W+Y3GiyXwGeDNv9XwLTg1Dld12Up3mvOZ/xj3u/wDS8Zt6G8MNvXIwWMq9fUo/Qsp88/Fr4Fh6aTw5rR1+Zmpz6GG9/lgWIhD0vGci6mUWIgPESJ5PTSbqsnQXniTjrXxdDaOIdPYIJpyrCq9Z6Nxj0uuUdNWTcmLm3Y4fHfSDiVHNtwMGb9TKWbbi/V8Gs+3Ls5vnY+xYkE2qSS6Gq+7WvgSL6xpl0gcK6Sa91/NCLxGsnXvyLcWmYi+mW2QxoZqdV02Qy7fVEJ7VjLnH3IP/AGnWjtBjjYMJZr1X1ad6M7y1nTmreOLXOz3IeQZ72xKulvuL56lNpwHHXx5MTlE57m4687zp79LYvRB9sZfSRFvnEX7PDf8ALP8AEIUBUzdb/DofpiX7rC8MT8YP0zLlhYS7sT8ZzmyrJdJjprfEv3WE/wDN+mIFb6p+wwf/AH/8pyakvH5WOtLfdf2GF3S2lfLGKrfVP2GF7+1f8xy7wVA636cf7nC97aP+Uhx8yCD2agaQiL8eXKhSO0Yl1EqvoSqz2PBXRiaxXV8jPY9j2nEo3BRWtZeqvDX4HVjuiVv9563+FKPmLhCai8jWMX1D0t3ZQspVKjuq3J9OTouw1wdi9voSonq+brTLsM+iEI4bpnSvw8P6gWE+lfFHcWFClKfQq9mg9U3/ADS8xTcchR6l+e4Dh/BF9x2ls2H7PxfmFbPh+z8X5ikefnhz5Rh3oSxtmxG6te6lFfM9d9nw/ZXi/M5G99hmq4mG240zhk3HrXSurUudG446hJfqv5/Uo4T9lkWLOjdfkCHFm6Yd8uyra8NDbK6hPW1lWn0PwYdpwsbDS4imq1Sq2K8SWtX4gNRquTXcy3G9W250bUqLRtaNrvErnrmMQToCtliU7OdM2Uni15/VlVCU600VK9+gzs+7sTEdsYSWmbTiku1gYKVap59uZlLY1ydOrVDsdlxOI8OMG5J5qn3eVa6U69B2O58fVpc8rkS4fl56exyjms+zyBFLop8D0eBufGlqlDtafgonQ2bcsF/eScn0L1V5k3cxrM15HgZZrLs+Ytibuw5dMX0ryPfbNuzCw5OSrKqolKkkvhma4uwYE004R7lRrsa0M7uLlx8p2vYJ4ebzj7S+q5Cjij6otx4SbacurR07a6nN2z0QwZxdsrJ8pKPq06JQrR91Dn1zn07c9b9vncsMynBo6m9N24uzTsxF0uMl92cemL71VchFmN5dM0pIqMSgjGeG0ZjpmqMFSEDQ1IAgSPrzwYN1cI1/woOFhQg24QjFvVpJGKmG89UfPptYgbxTiB4ghTamFTFOIFYgiU2phWIJ3hUxCnFiFuIJXhvEKcWIHiCamHiEh6GewYMpXOCrq6VSb60hqFsVSKSXQkkvgK8QPELCmcRRkqSSa6Gqo5O2bjhiScoSsrqkqxr0pZU5j14bxIV5ue6sVTsscqZpqlsk+tnR2Tc0sniNJV+6s2l2rL5nTvDeXaZF8DZcPDpbFKlXz5/PKiqxjiCt4eIZi01eS8V4hOISFM8Ql4txCcQQpi8l4vxCcQQpi8FxhxCXiLQ23ZsPGg4YkVKL5Pk+lPVPrR4zeXofiRrLAkpr2JUjNLqekvge0vJcN5azvcfJcfBnhzlCcXGUcnF5NGR9O3vurC2qFJ5SVbZr70fOPUeB3rujF2aXrqsXlGUfuvqfQ+p/E57zHbnvOnMlhpmEoNDJGZjpnUKEGeFEhI36x9HWIWvFlMtcet8wxeG8VuDeAzeHiC15LwGuIHiCt4eIUM8QN4rxA8QBq8l4rxA8QJTV4bxVTDeCmuIHiCt4bwtNcQPEFLw8QhTXEJeLcQKxAGbyXi3EJxBA1eS8WuJcItM3huFriXCFM3BuFrw3EgZUw3iymG8Qre8piKMk4ySaaaaeaafJlLwXki5rhbb6K4M6vClKDzdH60eyjzXied2jcO1Qq3huSVc4tTTXTROtO499cG8zvx5rpz8vWPmNvWvAh9NqiDw1/q4axCyxBRTCpnR5zamBzF1iBcwN+ITiC15Lyhq8N4pxA8QJTV4bxXiE4gKbvDxBO8N4U2sQPEFFMspgNcQKmK8QPEAavDeKcQPEAbvJeK3hvAb4hOIK3hvAZ4hLxa8PEAZvDcK8QPEAavDeK8QnEAavCpit4bwpriEuFryXgM3BvFrg3AM3kFuIQg4NxFIhCMLKQbyEArcS4hCg3kvIQrQ3kvIQCXkvIQMipFlMJAoXhuIQIN4byEAN4byEAN5LyEDSXhvCQCKYbyEAN5LyEAN4byEANwbgECjeFTIQCXkIQD//2Q==",
      //             width: double.infinity,
      //             height: 200,
      //           ),
      //         ),
      //         Row(
      //           children: [
      //             IconButton(
      //                 onPressed: () {},
      //                 icon: const Icon(
      //                   Icons.favorite_outline_rounded,
      //                 )),
      //             IconButton(
      //                 onPressed: () {},
      //                 icon: const Icon(
      //                   Icons.comment_bank_outlined,
      //                 )),
      //             IconButton(
      //               onPressed: () {},
      //               icon: const Icon(Icons.send),
      //             ),
      //           ],
      //         ),
      //       ],
      //     ),
      //      Column(
      //       mainAxisAlignment: MainAxisAlignment.start,
      //       crossAxisAlignment: CrossAxisAlignment.center,
      //       children: [
      //         Container(
      //           color: Colors.indigoAccent,
      //           child: Image.network(
      //             "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBESERISEhYYGBgYGRERGBkaGBgSGBISGBgZGRgYGBgcIS4lHB4rHxgYJjgmKy8xNTU1GiQ7QDs0Py40NTEBDAwMEA8QGhESHjQsISE0NDQ0NDE0NDQ0NDE0MTE0NDQ0NDQ0NDQ0MTQ0NDQxNDQ0NDQ0NDQ0NDQxNDQ0MTQ0NP/AABEIALcBEwMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAABBAACAwUGB//EAEIQAAIBAgMEBgcEBwgDAAAAAAABAhESAyExBAUTQVFhcYGR0QYiUpKhscEUMtLwFUJDU2KC4SMzcpOiwtPxY6Pi/8QAGQEBAQEBAQEAAAAAAAAAAAAAAAECAwQF/8QAHxEBAQEBAQACAwEBAAAAAAAAABEBAhIDQSFRYRMx/9oADAMBAAIRAxEAPwDJRComiiFRPq18aM1ENpqohtFIytComiiWtFWM7QqJoohtFIzUQ2mlpa0lVkohUTVRCoikZWllEuohoSrFLSWmtpLRSM7Q2mlpLRSM7SWmtpLSUjK0NDWhLRVjKha0vaS0UilpLS9obRVZ2ktNLSWijO0lpraS0lIyUQ2mloaCqztDaaWhtJVjG0htaAUjmKJZRNFEKiarnFFEKiaKIUhSKWhUS6iFRFIpaRRNFENoqxnQtaXUSyiKRlaG01oFRJVjK0sol1ENopFLSWmlobSUjOhKGtpLRVjO0NppaS0UjO0lppaS0lIztJaa0JQVYytJaa0JQUjK0NppQNpKRlaS01UQqApGSgS02UTh7/8ASPB2SsPv4nsJ5Ryyvf6vZr8yelzHXtORtnpJsmFGvEjN6KMGptvtTou9ngd5ekG1bQnGc6QesIKyD6mlnJdrZy0T035e8fptH9z44iT7/VIeF4jIKeX1pRLKJoohtN1zjO0Kia2kUSUiiiFRNFEKiPRFFENpoollEeiMrQqJraG0no8srQqJraS0ejyzoG0X/SWBdbeu39Xx+ug3CUZKsWmulNNeKG3FzMVtDaDHx4YdHOSjXSr1OJtXpDRyWHBNaKUq59dvQMzd/wCGzHctJaePW2zUnJznWXrNqTWfdyOrsu+ZxosT1ksqpes101rnT8s1vO4mdY7lpLTDZ944c6Z0rlV0Su9nWteea0G4NPRp9jT+RhqM7SWkw8aE5SjGSbjk0nVpm1oIxtJabWgtJVjK0NpraSgpGVobTShKCrGdpXFnGEXKbUYrNttRSXS29Di+knpPhbJ6kVfitJqH6sU9HN8tPurPsTqfNt470x9pldjTlLOqWkI9FsFkte0VfL1XpH6Y3KWFsjaWali6Nroh0f4vDpPFSk288+fawFWyNZyNSssRIyljdBg5GN6/Tecfttx2EXqQz61vzj309sxVrOb1/Xl5g2beePh1sm6OuT9ZdylWhhJPu8a/mprDsPc8Dt7L6Qyp/awXbCq/0utfE6D33gUTi26/wtW9taV7EeUnGoFNLl3md5xfWvbT2xJ0glPJSTU4U+dUa4e0xrSVE8s61i66Ul9HzPD4UtHpTmlp0M6mDvOKSuiq5JuKj660d0Xl59RjeWs6zXrlEjVNcu3Kp5ae1yxM1NLK1xT4ba6JVea7zKSadGmuqlMhnH9N6ewS5d/cGh4t0/PIYe8MVwsc5U01z8dX4jxv7Pb1EsaCurKKtzlWSVvb0Hnd671eJXDw8oaN88TyXV/0cxrPQkvz/Q1zzmflnrrdVeJlR9oYY84OsJShWjybVe2moXFPMylA2y0x8ec3WcnJ0pVtvKuSz7zGjNIwVaB5UDLNRkxiMykPzyRssKdjxFFuKai2v1W9K9HaKsZXNaZxqm11/ll8LanB1g5Rejo7aru7wxgm669Wa+X0Kyg1yy8fiRV4TTaaya0p5mz2jEatc5taNXSpTopUSnjQgnWSr0asWntr5ZLrzk/IEdbB2rEw/uTcVzzovDQbwPSGUVSThPr0fPNtZHlp7Q5vNunWXjiImzWsuPX7P6QUcuKqrWNmvZRvNdf5Qx/SWNP7ODu/ieS7UtTyM9rjBZunb9FzEsfflMoRTfTLyRnc5az1r1eN6SYkE3iTsTeqSk1lpGNKtaVOBvD0yx5QswpTjm3e5O5rqXLPpb7Eea2naZ4knKcqv5dSXIxlNHPrrPp15537XnJtuUm2222223JvVtvNsrKRjLEKSlU57065yvPF6DKU2wMFTG7WsyIwEYCKNSAoQK7ez7zmlRvul6y8zp7PvJOl6745rwPKqpeGJJaN+J15+Xccuvh517WGLGSrF1/PQGMo1oePhtc1zT7RzA3xiR+8lJd6fidM+bHLfg36elVG+w1hO3NNrl0VODDfmG/vQlHspJfQZjvbBf61O1PyNe+f2xvx9Z9Oy8eDpWNOtaPtX1XhzLTxVVrDbtedHna+hP6nMhtmE9Jx8UhiE06OLT7HUudJvOm1tC0kn3dIZ40ElRyrzi46fzV+FBfN5shank1CVdA2i8MtDeOPTUvog0YFFGc8eT0y+ZRKXSyejyZoirSaM4xkXSHo8NYZUzOjsW1xUaOUVyq0oZPX16Zd+WWfI5E5001Fpwk9XUz1uavObhzbtvw8uHFtqqbooReetKa/Dopoc7H2rFmqSlK3W1NpV7OZpwwSnCLo5JErUKrCZfhNmn2qC517EZYu8sOPT8h6XwPDoqvJdeQptG3KOUM+t6dy5ie2bwc31clyXmIYmPUxvbefGvj47k23mxWU2aQdWlVd+SKYiSbzr1rT4nPeq6ZxmKNsq0yzaBRv80M10ijAaRg3oq9mYKJEGdCKJdyK3AS0DRHIFwEqyAqQA3g4h0vs0v3D8Zr6Bexy/cP3p+RqJcc64Nx0fsMueCvekXjsUv3Mffl5ialxzL2FTZ1VsfThR96XmSWxwWsILnnOX4hC45ikGE6aOj6sjpLZsL+D3v6l44GDX9n739SzU9YWwtvxIZrEl2NuXweQ3DfeKtZRf8vlQ1hs+D/4fH/6GYbBhNZQwn2Jv/cazOv2zu8/eMMPf8ucYvsrHzGIb8VaOHhJP6G8N2w9iHuL5m/2GDVHCHuRX0LOmLwzhvjByurGrazWVVTmq9KHcPbMNqqlGnaL/YMP2Y+5DyNFseE9YQfbCH4TWevtN8/TZ7Zh+1HxRR7bD2o+8iv2LB/d4f8Alw/CH7FhexBfyQ8i/lPwH2vD9uHvR8zPG3jgxX3ot9Ckn/0b/ZcP2Ie5D8JX7BhtafCPkTauRxMbblN1c12VVF3GEtphRu5eKTfidmexRWj/ANMPwlHsv8T8I+RmatxwcTaqqql3KmnbWvwFpYizbf1bPRT2RNNNt9qg/oYS3bh/lQ8ibzredY89iTVXStOvUrU7s9gguX+mD/2mctjjX7q92H4TPnVzrHGkqJO5Z55OrXalzKSlHk/z1Ha+yw9mPuxX0M57NFaJeCMzWvWOZolW3PxXkWjKCVU7u7JD6wY+zEnAXsLwZI1nWOe9sk8k0l8CicK5y68tPH+h0HgrlFfHzKKDT0+fmIeiTxoLSKfW238MjKWMnkkl3JHSlGT6fF+ZVOa0lPo+9LTxELjnXrn5/LILa6V8Pyh9QmtK/EDhLofgFuOdd1gOhwpey/BkCF0sR+0/Fk4U/Zl4M78YPr+JtCEvyzt/n/Xn/wBf4808CXsy91+RVwS1XwPXwgaxi+vxH+X9P9/48lDYcSWkH3q35jOHujEeqjHtfkeoUe02jBdHwNZ8WJvza8zDcr5zXcqjOHubDX3nKXgjv8N8rV3V8i3Cll93woXOMZ35enKwt34MH9xV6/W+Y3GiyXwGeDNv9XwLTg1Dld12Up3mvOZ/xj3u/wDS8Zt6G8MNvXIwWMq9fUo/Qsp88/Fr4Fh6aTw5rR1+Zmpz6GG9/lgWIhD0vGci6mUWIgPESJ5PTSbqsnQXniTjrXxdDaOIdPYIJpyrCq9Z6Nxj0uuUdNWTcmLm3Y4fHfSDiVHNtwMGb9TKWbbi/V8Gs+3Ls5vnY+xYkE2qSS6Gq+7WvgSL6xpl0gcK6Sa91/NCLxGsnXvyLcWmYi+mW2QxoZqdV02Qy7fVEJ7VjLnH3IP/AGnWjtBjjYMJZr1X1ad6M7y1nTmreOLXOz3IeQZ72xKulvuL56lNpwHHXx5MTlE57m4687zp79LYvRB9sZfSRFvnEX7PDf8ALP8AEIUBUzdb/DofpiX7rC8MT8YP0zLlhYS7sT8ZzmyrJdJjprfEv3WE/wDN+mIFb6p+wwf/AH/8pyakvH5WOtLfdf2GF3S2lfLGKrfVP2GF7+1f8xy7wVA636cf7nC97aP+Uhx8yCD2agaQiL8eXKhSO0Yl1EqvoSqz2PBXRiaxXV8jPY9j2nEo3BRWtZeqvDX4HVjuiVv9563+FKPmLhCai8jWMX1D0t3ZQspVKjuq3J9OTouw1wdi9voSonq+brTLsM+iEI4bpnSvw8P6gWE+lfFHcWFClKfQq9mg9U3/ADS8xTcchR6l+e4Dh/BF9x2ls2H7PxfmFbPh+z8X5ikefnhz5Rh3oSxtmxG6te6lFfM9d9nw/ZXi/M5G99hmq4mG240zhk3HrXSurUudG446hJfqv5/Uo4T9lkWLOjdfkCHFm6Yd8uyra8NDbK6hPW1lWn0PwYdpwsbDS4imq1Sq2K8SWtX4gNRquTXcy3G9W250bUqLRtaNrvErnrmMQToCtliU7OdM2Uni15/VlVCU600VK9+gzs+7sTEdsYSWmbTiku1gYKVap59uZlLY1ydOrVDsdlxOI8OMG5J5qn3eVa6U69B2O58fVpc8rkS4fl56exyjms+zyBFLop8D0eBufGlqlDtafgonQ2bcsF/eScn0L1V5k3cxrM15HgZZrLs+Ytibuw5dMX0ryPfbNuzCw5OSrKqolKkkvhma4uwYE004R7lRrsa0M7uLlx8p2vYJ4ebzj7S+q5Cjij6otx4SbacurR07a6nN2z0QwZxdsrJ8pKPq06JQrR91Dn1zn07c9b9vncsMynBo6m9N24uzTsxF0uMl92cemL71VchFmN5dM0pIqMSgjGeG0ZjpmqMFSEDQ1IAgSPrzwYN1cI1/woOFhQg24QjFvVpJGKmG89UfPptYgbxTiB4ghTamFTFOIFYgiU2phWIJ3hUxCnFiFuIJXhvEKcWIHiCamHiEh6GewYMpXOCrq6VSb60hqFsVSKSXQkkvgK8QPELCmcRRkqSSa6Gqo5O2bjhiScoSsrqkqxr0pZU5j14bxIV5ue6sVTsscqZpqlsk+tnR2Tc0sniNJV+6s2l2rL5nTvDeXaZF8DZcPDpbFKlXz5/PKiqxjiCt4eIZi01eS8V4hOISFM8Ql4txCcQQpi8l4vxCcQQpi8FxhxCXiLQ23ZsPGg4YkVKL5Pk+lPVPrR4zeXofiRrLAkpr2JUjNLqekvge0vJcN5azvcfJcfBnhzlCcXGUcnF5NGR9O3vurC2qFJ5SVbZr70fOPUeB3rujF2aXrqsXlGUfuvqfQ+p/E57zHbnvOnMlhpmEoNDJGZjpnUKEGeFEhI36x9HWIWvFlMtcet8wxeG8VuDeAzeHiC15LwGuIHiCt4eIUM8QN4rxA8QBq8l4rxA8QJTV4bxVTDeCmuIHiCt4bwtNcQPEFLw8QhTXEJeLcQKxAGbyXi3EJxBA1eS8WuJcItM3huFriXCFM3BuFrw3EgZUw3iymG8Qre8piKMk4ySaaaaeaafJlLwXki5rhbb6K4M6vClKDzdH60eyjzXied2jcO1Qq3huSVc4tTTXTROtO499cG8zvx5rpz8vWPmNvWvAh9NqiDw1/q4axCyxBRTCpnR5zamBzF1iBcwN+ITiC15Lyhq8N4pxA8QJTV4bxXiE4gKbvDxBO8N4U2sQPEFFMspgNcQKmK8QPEAavDeKcQPEAbvJeK3hvAb4hOIK3hvAZ4hLxa8PEAZvDcK8QPEAavDeK8QnEAavCpit4bwpriEuFryXgM3BvFrg3AM3kFuIQg4NxFIhCMLKQbyEArcS4hCg3kvIQrQ3kvIQCXkvIQMipFlMJAoXhuIQIN4byEAN4byEAN5LyEDSXhvCQCKYbyEAN5LyEAN4byEANwbgECjeFTIQCXkIQD//2Q==",
      //             width: double.infinity,
      //             height: 200,
      //           ),
      //         ),
      //         Row(
      //           children: [
      //             IconButton(
      //                 onPressed: () {},
      //                 icon: const Icon(
      //                   Icons.favorite_outline_rounded,
      //                 )),
      //             IconButton(
      //                 onPressed: () {},
      //                 icon: const Icon(
      //                   Icons.comment_bank_outlined,
      //                 )),
      //             IconButton(
      //               onPressed: () {},
      //               icon: const Icon(Icons.send),
      //             ),
      //           ],
      //         ),
      //       ],
      //     ),

      //   ],
      // ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  color: Color.fromARGB(255, 53, 55, 67),
                  child: Image.network(
                    "https://images.news18.com/ibnlive/uploads/2016/09/Triumph-Daytona-675R-Feature.jpg?impolicy=website&width=360&height=240g",
                    width: double.infinity,
                    height: 200,
                  ),
                ),
                Row(
                  children: [
                    IconButton(
                        onPressed: () {
                          setState(() {
                            _ispost1like = !_ispost1like;
                          });
                        },
                        icon: _ispost1like
                            ? const Icon(
                                Icons.favorite_rounded,
                                color: Colors.red,
                              )
                            : const Icon(Icons.favorite_outline_rounded)),
                    IconButton(
                        onPressed: () {
                          setState(() {
                            Icons.bookmark_add_outlined;
                          });
                        },
                        icon: const Icon(
                          Icons.comment_bank_outlined,
                        )),
                    IconButton(
                      onPressed: () {},
                      icon: const Icon(Icons.send),
                    ),
                    const Spacer(),
                    IconButton(
                        onPressed: () {
                          setState(() {});
                        },
                        icon: const Icon(
                          Icons.bookmark_add_outlined,
                        )),
                  ],
                ),
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  color: const Color.fromARGB(255, 53, 55, 67),
                  child: Image.network(
                    "https://images.91wheels.com/news/wp-content/uploads/2022/08/BeFunky-collage-1-12.jpg?w=1080&q=65",
                    width: 900,
                    height: 200,
                  ),
                ),
                Row(
                  children: [
                    IconButton(
                        onPressed: () {
                          setState(() {
                            _ispost2like = !_ispost2like;
                          });
                        },
                        icon: _ispost2like
                            ? const Icon(
                                Icons.favorite_rounded,
                                color: Colors.red,
                              )
                            : const Icon(Icons.favorite_outline_rounded)),
                    IconButton(
                        onPressed: () {},
                        icon: const Icon(
                          Icons.comment_bank_outlined,
                        )),
                    IconButton(
                      onPressed: () {},
                      icon: const Icon(Icons.send),
                    ),
                    const Spacer(),
                    IconButton(
                        onPressed: () {},
                        icon: const Icon(
                          Icons.bookmark_add_outlined,
                        )),
                  ],
                ),
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  color: const Color.fromARGB(255, 53, 55, 67),
                  child: Image.network(
                      "https://media.istockphoto.com/id/1691787654/photo/sunset-over-an-open-road-and-a-man-with-a-helmet-riding-a-motorbike.webp?b=1&s=170667a&w=0&k=20&c=ET_9i88qF2Mu15S6AnDPEcKRagqQbM0dvQIBF0xedf4=",
                      width: double.infinity,
                      height: 200),
                ),
                Row(
                  children: [
                    IconButton(
                        onPressed: () {
                          setState(() {
                            _ispost3like = !_ispost3like;
                          });
                        },
                        icon: _ispost3like
                            ? const Icon(
                                Icons.favorite_rounded,
                                color: Colors.red,
                              )
                            : const Icon(Icons.favorite_outline_rounded)),
                    IconButton(
                        onPressed: () {},
                        icon: const Icon(
                          Icons.comment_bank_outlined,
                        )),
                    IconButton(
                      onPressed: () {},
                      icon: const Icon(Icons.send),
                    ),
                    const Spacer(),
                    IconButton(
                        onPressed: () {},
                        icon: const Icon(
                          Icons.bookmark_add_outlined,
                        )),
                  ],
                ),
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  color: const Color.fromARGB(255, 53, 55, 67),
                  child: Image.network(
                      "https://images.91wheels.com/news/wp-content/uploads/2022/08/BeFunky-collage-1-12.jpg?w=1080&q=65",
                      width: double.infinity,
                      height: 200),
                ),
                Row(
                  children: [
                    IconButton(
                        onPressed: () {
                          setState(() {
                            _ispost4like = !_ispost4like;
                          });
                        },
                        icon: _ispost4like
                            ? const Icon(
                                Icons.favorite_rounded,
                                color: Colors.red,
                              )
                            : const Icon(Icons.favorite_outline_rounded)),
                    IconButton(
                        onPressed: () {},
                        icon: const Icon(
                          Icons.comment_bank_outlined,
                        )),
                    IconButton(
                      onPressed: () {},
                      icon: const Icon(Icons.send),
                    ),
                    const Spacer(),
                    IconButton(
                        onPressed: () {},
                        icon: const Icon(
                          Icons.bookmark_add_outlined,
                        )),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
