import 'package:flutter/material.dart';
import 'package:cloneflix/image .dart';

class Assignment8 extends StatelessWidget {
  const Assignment8({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black87,
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: Image.asset(
                  "assets/N_logo.jpeg",
                  height: 45,
                ),
              ),
              Image.asset(
                "assets/Netflix_logo.jpg",
                height: 45,
                width: 120,
              ),
              const Spacer(),
              const Padding(
                padding: EdgeInsets.only(left: 110, right: 15),
                child: Icon(Icons.menu_outlined, size: 30, color: Colors.white),
              )
            ],
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              // crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(
                  width: 30,
                ),
                const Row(children: [
                  Padding(
                      padding: EdgeInsets.only(
                        left: 15,
                        top: 10,
                        bottom: 10,
                      ),
                      child: Text("Movies",
                          style: TextStyle(
                            fontSize: 30,
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                          ))),
                ]),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      const SizedBox(
                        width: 20,
                      ),
                      Img(url: "https://media5.bollywoodhungama.in/wp-content/uploads/2023/10/12th-Fail.jpg")
                          .newImg(),
                      Img(url: "https://dbdzm869oupei.cloudfront.net/img/posters/preview/91008.png")
                          .newImg(),
                      Img(url: "https://assetscdn1.paytm.com/images/catalog/product/H/HO/HOMSHERLOCK-HOLHK-P63024784A1CC1B/1563111214645_0..jpg")
                          .newImg(),
                      Img(url: "https://assets-in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC/et00311762-lmdexnggxy-portrait.jpg")
                          .newImg(),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                const Row(children: [
                  Padding(
                      padding: EdgeInsets.only(
                        left: 15,
                        top: 10,
                        bottom: 10,
                      ),
                      child: Text("Series",
                          style: TextStyle(
                            fontSize: 30,
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                          ))),
                ]),
                const SizedBox(
                  width: 10,
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(children: [
                    const SizedBox(
                      width: 20,
                    ),
                    Img(url: "https://m.media-amazon.com/images/M/MV5BNWQ3MDdkNGItMzNkMy00MDkwLWIwNzYtYzIxNzBhYWUyOGZiXkEyXkFqcGdeQXVyODE5NzE3OTE@._V1_QL75_UY562_CR9,0,380,562_.jpg")
                        .webbImg(),
                    Img(url: "https://www.tallengestore.com/cdn/shop/products/PeakyBlinders-NetflixTVShow-ArtPoster_125897c4-6348-41e8-b195-d203700ebcca.jpg?v=1619864555")
                        .webbImg(),
                    Img(url: "https://m.media-amazon.com/images/M/MV5BZjg2YjU0MTEtMGYyNy00OWI1LTk2MDgtMDFkNmMwYWFjZTY5XkEyXkFqcGdeQXVyMTEzMTI1Mjk3._V1_QL75_UY562_CR35,0,380,562_.jpg")
                        .webbImg(),
                    Img(url: "https://pbs.twimg.com/media/Ftg4jBUWIAA6WH5.jpg:large")
                        .webbImg(),
                    Img(url: "https://m.media-amazon.com/images/M/MV5BMDZkYmVhNjMtNWU4MC00MDQxLWE3MjYtZGMzZWI1ZjhlOWJmXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_.jpg")
                        .webbImg(),
                  ]),
                ),
                const SizedBox(
                  height: 20,
                ),
                const Row(children: [
                  Padding(
                      padding: EdgeInsets.only(
                        left: 15,
                        top: 10,
                        bottom: 10,
                      ),
                      child: Text("Most Popular",
                          style: TextStyle(
                            fontSize: 30,
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                          ))),
                ]),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(children: [
                    const SizedBox(
                      width: 20,
                    ),
                    Img(url: "https://dbdzm869oupei.cloudfront.net/img/posters/preview/91008.png")
                        .webbImg(),
                    Img(url: "https://upload.wikimedia.org/wikipedia/en/b/bd/ThePopesExorcistPoster.png")
                        .webbImg(),
                    Img(url: "https://upload.wikimedia.org/wikipedia/en/4/4f/Insidious_the_red_door.png")
                        .webbImg(),
                    Img(url: "https://upload.wikimedia.org/wikipedia/en/6/6e/Skyscraper_%282018%29_film_poster.png")
                        .webbImg(),
                  ]),
                ),
                const Row(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                  ],
                )
              ]),
        ));
  }
}
