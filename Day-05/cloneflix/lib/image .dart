import 'package:flutter/material.dart';

class Img {
  String url;

  Img({this.url = ""});
  Widget newImg() {
    Widget img = Padding(
        padding: const EdgeInsets.only(right: 20),
        child: ClipRRect(
            borderRadius: BorderRadius.circular(15),
            child:
                Image.network(url, height: 300, width: 190, fit: BoxFit.fill)));
    return img;
  }

  Widget webbImg() {
    Widget img = Padding(
        padding: const EdgeInsets.only(right: 20),
        child: ClipRRect(
            borderRadius: BorderRadius.circular(15),
            child:
                Image.network(url, height: 200, width: 130, fit: BoxFit.fill)));
    return img;
  }

  // Widget webImg() {
  //   Widget img = Padding(
  //       padding: const EdgeInsets.only(right: 20),
  //       child: ClipRRect(
  //           borderRadius: BorderRadius.circular(15),
  //           child:
  //               Image.network(url, height: 00, width: 90, fit: BoxFit.fill)));
  //   return img;
}

    //              Container(
    //                     margin: const EdgeInsets.all(15),
    //                     child: ClipRRect(
    //                       borderRadius: BorderRadius.circular(15),
    //                       child: Image.network(
    //                         "https://assets-in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC/et00311762-lmdexnggxy-portrait.jpg",
    //                         fit: BoxFit.cover,
    //                         width: 180,
    //                         height: 300,
    //                       ),
    //                     ),
    //                   ),